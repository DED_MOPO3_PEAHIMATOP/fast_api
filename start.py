import pathlib
import click
import mlflow
import os
from mlflow.models.signature import infer_signature

from src import first_preparation_no_click
from src import creating_train_data_no_click
from src import train_no_click

REPO_PATH = pathlib.Path.cwd()


@click.command()
def cli():
    first_preparation_no_click()
    creating_train_data_no_click(
        input_path_1=REPO_PATH / 'data/interim/osteo_fractures_01.csv',
        input_path_2=REPO_PATH / 'data/interim/osteo_fractures_02.csv',
        output_path=REPO_PATH / 'data/interim/fractures_concated.csv'
    )
    # mlflow.set_tracking_uri('http://127.0.0.1:5000')
    mlflow.set_experiment('sklearn_catboost')
    os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'minio:8000'
    os.environ['MLflow_TRACKING_USERNAME'] = 'minioadmin'

    # mlflow.autolog()
    mlflow.sklearn.autolog()
    with mlflow.start_run():
        # cb, X_train, pred = train_no_click()
        lg, X_train, pred = train_no_click()
        singature = infer_signature(X_train, pred)
        # mlflow.catboost.log_model(cb, 'osteoporosis_cb', singature=singature)
        mlflow.sklearn.log_model(lg, 'osteoporosis_lg', signature=singature)
        autolog_run = mlflow.last_active_run()
    pass


if __name__ == '__main__':
    cli()
