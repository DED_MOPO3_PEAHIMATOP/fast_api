import pandas as pd
import click


def remoove_columns(df: pd.DataFrame, cols: list[str]) -> None:
    df.drop(cols, axis=1, inplace=True)


# меняем Nan на 0, не Nan на 1
def prep_nan(value: str | None) -> int:
    if value is None:
        return 0
    return 1


# получаем возраст из строки вида 1933 (84)
def prep_year(year: str) -> int:
    year = year.replace("(", ' ').replace(")", ' ')
    return int(year.split()[1])


def prep_second_df(df: pd.DataFrame) -> None:
    # удаляем колонки в которых одни наны
    # и которых нет в первом датасете
    drop_cols = [
        'Стаж СД', 'спондиллез', 'спондилоартроз', 'Протромбин', 'витамин Д',
        'АЛТ', 'АСТ', 'Калий', 'Натрий', 'ЭКГ  (блокады)',
        'римт  (синусовый/аритмия)',  'зоны гипо-, акинезии (ЭхоКГ)',
        'конеч. Диастолический диаметр ЛЖ (ЭхоКГ)',
        'конеч.систолический диаметр ЛЖ (ЭхоКГ)', 'МНО',
        'Билирубин прямой', 'Unnamed: 31', 'Unnamed: 32',
        'экстрасистолы', 'толщина задней стенки ЛЖ (ЭхоКГ)',
        'Толщина МЖП (ЭхоКГ)', 'толщина ЛП (ЭхоКГ)', 'Толщина ПП (ЭхоКГ)',
        'давление в легочной а.', 'застойные явления (Rg легких)',
        'син. Тахикардия', 'ФВ (ЭхоКГ)'
        ]
    df.drop(drop_cols, axis=1, inplace=True)
    # заодно меняем имена колонок
    df.rename(columns={
        ', бедр ( R': 'Бедро®', ', бедр ,L)': 'Бедро(L)',
        'ЭКГ (ГЛЖ': 'ЭКГ (ГЛЖ)', 'ДОА (степень)': 'ДОА'
    },
      inplace=True)

    cols = ['ДОА', 'ДОА коленных суставов', 'грыжи дисков (уровень)',
            'о/хондроз', 'ДДЗП',  'ЭКГ (ГЛЖ)', 'ЭКГ (Нарушение реполяризации)']
    for col in cols:
        df[col] = df[col].apply(prep_nan)
    df['ДДЗП'] = df[
        ['грыжи дисков (уровень)', 'о/хондроз', 'ДДЗП']
    ].max(axis=1)
    df['ДОА'] = df[
        ['ДОА', 'ДОА коленных суставов']
    ].max(axis=1)
    df.drop(cols[1:-3], axis=1, inplace=True)
    # нужно получить возраст
    df['год рождения (возраст)'] = df['год рождения (возраст)'].apply(prep_year)

    pass


def creating_train_data_no_click(
        input_path_1: str,
        input_path_2: str,
        output_path: str
        ) -> None:
    """Функция производит очистку датасетов и формирует данные
      пригодные для обучения """
    fractures_01 = pd.read_csv(input_path_1)
    fractures_02 = pd.read_csv(input_path_2)

    remoove_columns(
        fractures_01,
        ['индекс коморбидности', 'индекс CIRS', 'БЛНПГ', 'Пост ФП',
         'наджел.экстр', 'АВблок1', 'Желуд.экстрасистолия', 'АВблок2ст',
         'ПароксизмФП', 'ФЗ низкая', 'ФЗ ниже средней', 'ФН средняя',
         'ФН высокая', 'ФВ более55%', 'ФВ 45-50%', 'ФВ 35-45%',
         'ФВ 25-35%', 'о/хондроз', 'ИММЛЖ', 'син. Тахикардия', 'ФВ']
         )

    prep_second_df(fractures_02)
    # слияем обработанные df
    fractures_concated = pd.concat((fractures_01, fractures_02),
                                   axis=0, ignore_index=True)
    fractures_concated['id'] = fractures_concated.index+1
    # обработка None
    cols = ['ТТГ', 'ИМТ', 'общий белок, г/л']
    for col in cols:
        fractures_concated.loc[fractures_concated[col].isna(), col] =\
                        fractures_concated[col].median()
    # Сохраняем подготовленный датасет
    fractures_concated.to_csv(output_path, index=False)
    pass


@click.command()
@click.argument("input_path_1", type=click.Path(exists=True))
@click.argument("input_path_2", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def creating_train_data(input_path_1: str,
                        input_path_2: str,
                        output_path: str) -> None:
    creating_train_data_no_click(input_path_1, input_path_2, output_path)
    pass


if __name__ == '__main__':
    creating_train_data()
