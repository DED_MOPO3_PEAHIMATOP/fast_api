import click
import pandas as pd
import pathlib
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score, StratifiedKFold, train_test_split
from sklearn.ensemble import RandomForestClassifier
from catboost import CatBoostClassifier
import warnings

warnings.filterwarnings("ignore")

REPO_PATH = pathlib.Path.cwd()

# Простое предсказание простым алгоритмом


n_splits = 7

skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)


def clf_scores(clf, X, targets):
    print('Предсказания с моделью', clf)
    for col in targets.columns:
        scores = cross_val_score(clf, X, targets[col], cv=skf, scoring='roc_auc')
        print(col, 'all scores', scores.round(2))
        print(' mean ', round(scores.mean(), 3), '\n')


def train_no_click():
    print('Start train on train model')
    df = pd.read_csv(REPO_PATH / 'data/interim/fractures_concated.csv')
    # выделим таргеты
    # колонки с переломами
    fracture_colls = df.filter(like='перелом').columns
    targets = df[fracture_colls].astype(int)
    # перелом в любом месте
    targets['перелом'] = targets.max(axis=1)
    X = df.drop(fracture_colls, axis=1).iloc[:, 1:]
    # clf_scores(GaussianNB(), X, targets)
    # clf_scores(SVC(random_state=42), X, targets)
    # clf_scores(DecisionTreeClassifier(random_state=42), X, targets)
    clf_scores(LogisticRegression(max_iter=1500, random_state=42), X, targets)
    # clf_scores(KNeighborsClassifier(), X, targets)
    # clf_scores(RandomForestClassifier(random_state=42), X, targets)
    # clf_scores(CatBoostClassifier(logging_level='Silent', random_seed=42), X, targets)
    X_train, X_test, y_train, y_test = train_test_split(X, targets['перелом'])
    # cb = CatBoostClassifier(logging_level='Silent', random_seed=42)
    # cb.fit(X_train, y_train)
    # pred = cb.predict(X_test)
    lg = LogisticRegression(max_iter=1500, random_state=42)
    lg.fit(X_train, y_train)
    pred = lg.predict(X_test)
    print('End train on train model')
    # return cb, X_train, pred
    return lg, X_train, pred


@click.command()
def train():
    train_no_click()
    pass

if __name__ == '__main__':
    train()